/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kamufirstcode;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import java.util.Random;
import javafx.scene.control.TextArea;


/**
 *
 * @author KaMu
 */
public class FXMLDocumentController implements Initializable {
     Connection Cone;
     
     String[] FistN = {"李","陳","廖","盧","蔡","趙","蕭"};
     String[] LastN ={"忠澤","泓羲","宣任","秉宗","力銘","志宮","元渝","聖哲"};
     String[] MAMA={"肥宅","帥哥","..不予至評"};
     String Now_Select = "select * from KaMu.COLLEAGUES where ID=0";
     PreparedStatement SUPpst;
     ResultSet SUPresult;
     
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button Btn_AddData;
    @FXML
    private Label Show_Data;
    @FXML
    private TextArea Show_DataArea;
    @FXML
    private Button Btn_SelectData;
    @FXML
    private Button Btn_UpData;
    @FXML
    private Button Btn_NextD;
    
    @FXML
    private void UpNextData(ActionEvent event) {
        Button btn = (Button)event.getSource();
        //搜尋當前的資料筆數
         try {
            if("Btn_UpData".equals(btn.getId()))
            {
                SUPresult.previous();
                Show_DataArea.appendText(SUPresult.getString(2)+ SUPresult.getString(3)+"是"+SUPresult.getString(4)+"\n");
               // Now_Select="select * from KaMu.COLLEAGUES where ID<="+result.getString(1);
            }
            else if("Btn_NextD".equals(btn.getId()))
            {
                SUPresult.next();   
                Show_DataArea.appendText(SUPresult.getString(2)+ SUPresult.getString(3)+"是"+SUPresult.getString(4)+"\n");
               // Now_Select="select * from KaMu.COLLEAGUES where ID>="+result.getString(1);
            }
           // Show_DataArea.setText("Yep");
         } catch (SQLException ex) {
             Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
             Show_DataArea.setText("Error!");
         }
   }
    //cursor
    @FXML
    private void handleButtonAction(ActionEvent event) {
        try {
           Cone =DriverManager.getConnection("jdbc:derby://localhost:1527/KaMuFirstCode_DB","KaMu","6078");
           SUPpst = Cone.prepareStatement(Now_Select);
           SUPresult = SUPpst.executeQuery();
           label.setText("Linked");
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("Filed");
        }
    }
    @FXML
    private int DataSer()
    {
        Show_DataArea.setText("");
        int rows=0;
        String Select = "select * from KaMu.COLLEAGUES";
         try {
             PreparedStatement pst = Cone.prepareStatement(Select);
             ResultSet result = pst.executeQuery();
             while(result.next())
             {
                 Show_DataArea.appendText(result.getString(2)+ result.getString(3)+"是"+result.getString(4)+"\n");
                rows++;
             }
             result.close();
             pst.close();
         } catch (SQLException ex) {
             Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
         }finally
         {
          Show_DataArea.appendText("查詢共有："+rows+"筆資料");
          return rows;   
         }
    }
    
    @FXML
    private int AddData()
    {
        int rows=0;
        Random ran = new Random();
        String insert = "insert into KaMu.COLLEAGUES values (?,?,?,?,?,?,?)";
         try {
             PreparedStatement pst = Cone.prepareStatement(insert);
             pst.setInt(1,ran.nextInt(9999999));
             pst.setString(2, FistN[ran.nextInt(FistN.length)]);
             pst.setString(3, LastN[ran.nextInt(LastN.length)]);
             pst.setString(4, MAMA[ran.nextInt(MAMA.length)]);
             pst.setString(5, MAMA[ran.nextInt(MAMA.length)]+"隊");
             pst.setString(6, "fathouse@8787.com.87");
             pst.setString(7,"87");
             rows = pst.executeUpdate();
             pst.close();
         } catch (SQLException ex) {
             Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
         }finally{
             if(rows !=0)
             {
                 Show_Data.setText("以新增資料："+ rows +"筆");
             }
             return rows;
         }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    
}
